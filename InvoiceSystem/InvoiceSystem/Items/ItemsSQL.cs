﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceSystem
{
    public class ItemsSQL
    {
        public ItemsSQL() { }

        public void Function()
        {
            //Sample of how to do a SQL statement
            //SELECT * FROM ItemDesc
            DataSet ds;
            int rows = 0;
            ds = clsDataAccess.ExecuteSQLStatement("SELECT * FROM ItemDesc", ref rows);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                //ItemCode PK
                string ItemCode = row[0].ToString();
                //ItemDesc
                string ItemDesc = row[1].ToString();
                //ItemCost
                Double.TryParse(row[2].ToString(), out double ItemCost);
            }

        }
    }
}
